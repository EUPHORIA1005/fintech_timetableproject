package timetableProject.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import timetableProject.model.Lesson;
import timetableProject.model.Teacher;
import timetableProject.repository.*;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class TeacherControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private TeacherRepository teacherRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private LessonRepository lessonRepository;

    @AfterEach
    void tearDown() {
        courseRepository.deleteAll();
        userRepository.deleteAll();
        teacherRepository.deleteAll();
        categoryRepository.deleteAll();
        lessonRepository.deleteAll();
    }

    @Test
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('petr', 'Petrov Petr Petrovich', 30)")
    void getAllTeachersSuccessForAdmin() throws Exception {
        Teacher teacher = new Teacher();
        teacher.setUsername("petr");
        teacher.setCourses(new ArrayList<>());
        teacher.setFullName("Petrov Petr Petrovich");
        teacher.setAge(30);

        List<Teacher> teacherList = new ArrayList<>();
        teacherList.add(teacher);

        String expectedContent = objectMapper.writeValueAsString(teacherList);

        this.mockMvc.perform(get("/api/teachers")
                        .with(user("admin").password("admin").roles("ADMIN")))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('petr', 'Petrov Petr Petrovich', 30)")
    void getAllTeachersSuccessForUser() throws Exception {
        Teacher teacher = new Teacher();
        teacher.setUsername("petr");
        teacher.setCourses(new ArrayList<>());
        teacher.setFullName("Petrov Petr Petrovich");
        teacher.setAge(30);

        List<Teacher> teacherList = new ArrayList<>();
        teacherList.add(teacher);

        String expectedContent = objectMapper.writeValueAsString(teacherList);

        this.mockMvc.perform(get("/api/teachers")
                        .with(user("petr").password("user").roles("TEACHER")))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('petr', 'Petrov Petr Petrovich', 30)")
    void getByUsernameSuccessForAdmin() throws Exception {
        Teacher teacher = new Teacher();
        teacher.setUsername("petr");
        teacher.setCourses(new ArrayList<>());
        teacher.setFullName("Petrov Petr Petrovich");
        teacher.setAge(30);

        String expectedContent = objectMapper.writeValueAsString(teacher);

        this.mockMvc.perform(get("/api/teachers/petr")
                        .with(user("admin").password("admin").roles("ADMIN")))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('petr', 'Petrov Petr Petrovich', 30)")
    void getByUsernameSuccessForUser() throws Exception {
        Teacher teacher = new Teacher();
        teacher.setUsername("petr");
        teacher.setCourses(new ArrayList<>());
        teacher.setFullName("Petrov Petr Petrovich");
        teacher.setAge(30);

        String expectedContent = objectMapper.writeValueAsString(teacher);

        this.mockMvc.perform(get("/api/teachers/petr")
                        .with(user("petr").password("user").roles("TEACHER")))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('petr', 'Petrov Petr Petrovich', 30)")
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('category', null)")
    @Sql(statements = "insert into courses (name, description, category, supervisorUsername, studentsCount, type)" +
            " values ('course', 'descr', 'category', 'petr', 20, 'offline' )")
    @Sql(statements = "insert into lessons (id, name, description, teacherUsername, dateTime, courseName) " +
            "values (1, 'lesson', 'descr', 'petr', '', 'course')")
    void getTeacherTimetableSuccessForAdmin() throws Exception {
        Lesson lesson = new Lesson();
        lesson.setId(1);
        lesson.setName("lesson");
        lesson.setDescription("descr");
        lesson.setTeacherUsername("petr");
        lesson.setDateTime("");
        lesson.setCourseName("course");

        List<Lesson> lessonList = new ArrayList<>();
        lessonList.add(lesson);

        String expectedContent = objectMapper.writeValueAsString(lessonList);

        this.mockMvc.perform(get("/api/teachers/timetable/petr")
                        .with(user("admin").password("admin").roles("ADMIN")))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('petr', 'Petrov Petr Petrovich', 30)")
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('category', null)")
    @Sql(statements = "insert into courses (name, description, category, supervisorUsername, studentsCount, type)" +
            " values ('course', 'descr', 'category', 'petr', 20, 'offline' )")
    @Sql(statements = "insert into lessons (id, name, description, teacherUsername, dateTime, courseName) " +
            "values (1, 'lesson', 'descr', 'petr', '', 'course')")
    void getTeacherTimetableSuccessForUser() throws Exception {
        Lesson lesson = new Lesson();
        lesson.setId(1);
        lesson.setName("lesson");
        lesson.setDescription("descr");
        lesson.setTeacherUsername("petr");
        lesson.setDateTime("");
        lesson.setCourseName("course");

        List<Lesson> lessonList = new ArrayList<>();
        lessonList.add(lesson);

        String expectedContent = objectMapper.writeValueAsString(lessonList);

        this.mockMvc.perform(get("/api/teachers/timetable/petr")
                        .with(user("petr").password("user").roles("TEACHER")))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    void createTeacherSuccessForAdmin() throws Exception {
        Teacher teacher = new Teacher();
        teacher.setUsername("petr");
        teacher.setCourses(new ArrayList<>());
        teacher.setFullName("Petrov Petr Petrovich");
        teacher.setAge(30);

        String requestContent = objectMapper.writeValueAsString(teacher);

        this.mockMvc.perform(post("/api/teachers/createTeacher")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestContent)
                        .with(user("admin").password("admin").roles("ADMIN")))
                .andExpect(status().isCreated());
    }

    @Test
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    void createTeacherForbiddenForUser() throws Exception {
        Teacher teacher = new Teacher();
        teacher.setUsername("petr");
        teacher.setCourses(new ArrayList<>());
        teacher.setFullName("Petrov Petr Petrovich");
        teacher.setAge(30);

        String requestContent = objectMapper.writeValueAsString(teacher);

        this.mockMvc.perform(post("/api/teachers/createTeacher")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestContent)
                        .with(user("petr").password("user").roles("TEACHER")))
                .andExpect(status().isForbidden());
    }

    @Test
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('petr', 'Petrov Petr Petrovich', 30)")
    void updateTeacherSuccessForAdmin() throws Exception {
        Teacher teacher = new Teacher();
        teacher.setUsername("petr");
        teacher.setCourses(new ArrayList<>());
        teacher.setFullName("Petrov Petr Petrovich");
        teacher.setAge(31);

        String requestContent = objectMapper.writeValueAsString(teacher);

        this.mockMvc.perform(put("/api/teachers/updateTeacher")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestContent)
                        .with(user("admin").password("admin").roles("ADMIN")))
                .andExpect(status().isOk());
    }

    @Test
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('petr', 'Petrov Petr Petrovich', 30)")
    void updateTeacherForbiddenForUser() throws Exception {
        Teacher teacher = new Teacher();
        teacher.setUsername("petr");
        teacher.setCourses(new ArrayList<>());
        teacher.setFullName("Petrov Petr Petrovich");
        teacher.setAge(31);

        String requestContent = objectMapper.writeValueAsString(teacher);

        this.mockMvc.perform(put("/api/teachers/updateTeacher")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestContent)
                        .with(user("petr").password("user").roles("TEACHER")))
                .andExpect(status().isForbidden());
    }

    @Test
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('petr', 'Петров Петр Петрович', 30)")
    void deleteTeacherSuccessForAdmin() throws Exception {
        this.mockMvc.perform(delete("/api/teachers/petr")
                        .with(user("admin").password("admin").roles("ADMIN")))
                .andExpect(status().isOk());
    }

    @Test
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('petr', 'Петров Петр Петрович', 30)")
    void deleteTeacherForbiddenForUser() throws Exception {
        this.mockMvc.perform(delete("/api/teachers/petr")
                        .with(user("petr").password("user").roles("TEACHER")))
                .andExpect(status().isForbidden());
    }

    @Test
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('petr', 'Петров Петр Петрович', 30)")
    void deleteAllSuccessForAdmin() throws Exception {
        this.mockMvc.perform(delete("/api/teachers")
                        .with(user("admin").password("admin").roles("ADMIN")))
                .andExpect(status().isOk());
    }

    @Test
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('petr', 'Петров Петр Петрович', 30)")
    void deleteAllForbiddenForUser() throws Exception {
        this.mockMvc.perform(delete("/api/teachers")
                        .with(user("petr").password("user").roles("TEACHER")))
                .andExpect(status().isForbidden());
    }
}