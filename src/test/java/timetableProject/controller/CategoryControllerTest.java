package timetableProject.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import timetableProject.model.Category;
import timetableProject.repository.CategoryRepository;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class CategoryControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private CategoryRepository categoryRepository;


    @AfterEach
    void tearDown() {
        categoryRepository.deleteAll();
    }

    @Test
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('parentCategory', null)")
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('category1', 'parentCategory')")
    void getCategoriesSuccessForAdmin() throws Exception {

        Category parentCategory = new Category();
        parentCategory.setCategoryName("parentCategory");
        parentCategory.setParentCategory(null);

        Category category = new Category();
        category.setCategoryName("category1");
        category.setParentCategory("parentCategory");


        List<Category> categoryList = new ArrayList<>();
        categoryList.add(parentCategory);
        categoryList.add(category);

        String expectedContent = objectMapper.writeValueAsString(categoryList);

        this.mockMvc.perform(get("/api/categories")
                        .with(user("admin").password("admin").roles("ADMIN")))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('parentCategory', null)")
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('category1', 'parentCategory')")
    void getByCategoriesSuccessForUser() throws Exception {

        Category parentCategory = new Category();
        parentCategory.setCategoryName("parentCategory");
        parentCategory.setParentCategory(null);

        Category category = new Category();
        category.setCategoryName("category1");
        category.setParentCategory("parentCategory");


        List<Category> categoryList = new ArrayList<>();
        categoryList.add(parentCategory);
        categoryList.add(category);

        String expectedContent = objectMapper.writeValueAsString(categoryList);

        this.mockMvc.perform(get("/api/categories")
                        .with(user("user").password("user").roles("TEACHER")))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    void getCategoriesNoContent() throws Exception {
        this.mockMvc.perform(get("/api/categories")
                        .with(user("user").password("user").roles("TEACHER")))
                .andExpect(status().isNoContent());
    }


    @Test
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('parentCategory', null)")
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('anotherParent', null)")
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('category1', 'parentCategory')")
    void updateSuccessForAdmin() throws Exception {
        Category category = new Category();
        category.setCategoryName("category1");
        category.setParentCategory("anotherParent");

        String requestContent = objectMapper.writeValueAsString(category);

        this.mockMvc.perform(put("/api/categories")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestContent)
                .with(user("admin").password("admin").roles("ADMIN"))
        ).andExpect(status().isOk());
    }

    @Test
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('parentCategory', null)")
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('anotherParent', null)")
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('category1', 'parentCategory')")
    void updateForbiddenForUser() throws Exception {
        Category category = new Category();
        category.setCategoryName("category1");
        category.setParentCategory("anotherParent");

        String requestContent = objectMapper.writeValueAsString(category);

        this.mockMvc.perform(put("/api/categories")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestContent)
                .with(user("user").password("user").roles("TEACHER"))
        ).andExpect(status().isForbidden());
    }

    @Test
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('parentCategory', null)")
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('category1', 'parentCategory')")
    void updateInvalid() throws Exception {
        Category category = new Category();
        category.setParentCategory("anotherParent");

        String requestContent = objectMapper.writeValueAsString(category);

        this.mockMvc.perform(put("/api/categories")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestContent)
                .with(user("admin").password("admin").roles("ADMIN"))
        ).andExpect(status().isBadRequest());
    }

    @Test
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('parentCategory', null)")
    void createSuccessForAdmin() throws Exception {
        Category category = new Category();
        category.setCategoryName("category1");
        category.setParentCategory("parentCategory");

        String requestContent = objectMapper.writeValueAsString(category);

        this.mockMvc.perform(post("/api/categories")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestContent)
                .with(user("admin").password("admin").roles("ADMIN"))
        ).andExpect(status().isCreated());
    }

    @Test
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('parentCategory', null)")
    void createForbiddenForUser() throws Exception {
        Category category = new Category();
        category.setCategoryName("category1");
        category.setParentCategory("parentCategory");

        String requestContent = objectMapper.writeValueAsString(category);

        this.mockMvc.perform(post("/api/categories")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestContent)
                .with(user("user").password("user").roles("TEACHER"))
        ).andExpect(status().isForbidden());
    }

    @Test
    void createInvalid() throws Exception {
        Category category = new Category();
        category.setParentCategory("parentCategory");

        String requestContent = objectMapper.writeValueAsString(category);

        this.mockMvc.perform(post("/api/categories")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestContent)
                .with(user("admin").password("admin").roles("ADMIN"))
        ).andExpect(status().isBadRequest());
    }

    @Test
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('parentCategory', null)")
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('category1', 'parentCategory')")
    void deleteSuccessForAdmin() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.delete("/api/categories/category1")
                        .with(user("admin").password("admin").roles("ADMIN")))
                .andExpect(status().isOk());
    }

    @Test
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('parentCategory', null)")
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('category1', 'parentCategory')")
    void deleteForbiddenForUser() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.delete("/api/categories/category1")
                        .with(user("user").password("user").roles("TEACHER")))
                .andExpect(status().isForbidden());
    }
}