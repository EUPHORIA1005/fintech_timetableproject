package timetableProject.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import timetableProject.model.Course;
import timetableProject.repository.*;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
class CourseControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private TeacherRepository teacherRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private LessonRepository lessonRepository;

    @AfterEach
    void tearDown() {
        courseRepository.deleteAll();
        userRepository.deleteAll();
        teacherRepository.deleteAll();
        categoryRepository.deleteAll();
        lessonRepository.deleteAll();
    }

    @Test
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('petr', 'Петров Петр Петрович', 30)")
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('category', null)")
    @Sql(statements = "insert into courses (name, description, category, supervisorUsername, studentsCount, type)" +
            " values ('course', 'descr', 'category', 'petr', 20, 'offline' )")
    void getAllCoursesSuccessForAdmin() throws Exception {

        Course course = new Course();
        course.setName("course");
        course.setDescription("descr");
        course.setCategory("category");
        course.setLessons(new ArrayList<>());
        course.setSupervisorUsername("petr");
        course.setStudentsCount(20);
        course.setType("offline");


        List<Course> courseList = new ArrayList<>();
        courseList.add(course);

        String expectedContent = objectMapper.writeValueAsString(courseList);

        this.mockMvc.perform(get("/api/courses")
                        .with(user("admin").password("admin").roles("ADMIN")))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('petr', 'Петров Петр Петрович', 30)")
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('category', null)")
    @Sql(statements = "insert into courses (name, description, category, supervisorUsername, studentsCount, type)" +
            " values ('course', 'descr', 'category', 'petr', 20, 'offline' )")
    void getAllCoursesSuccessForUser() throws Exception {

        Course course = new Course();
        course.setName("course");
        course.setDescription("descr");
        course.setCategory("category");
        course.setLessons(new ArrayList<>());
        course.setSupervisorUsername("petr");
        course.setStudentsCount(20);
        course.setType("offline");


        List<Course> courseList = new ArrayList<>();
        courseList.add(course);

        String expectedContent = objectMapper.writeValueAsString(courseList);

        this.mockMvc.perform(get("/api/courses")
                        .with(user("user").password("user").roles("TEACHER")))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    void getAllCoursesNoContent() throws Exception {
        this.mockMvc.perform(get("/api/courses")
                        .with(user("user").password("user").roles("TEACHER")))
                .andExpect(status().isNoContent());
    }

    @Test
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('petr', 'Петров Петр Петрович', 30)")
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('category', null)")
    @Sql(statements = "insert into courses (name, description, category, supervisorUsername, studentsCount, type)" +
            " values ('course', 'descr', 'category', 'petr', 20, 'offline' )")
    void getCourseByNameSuccessForUser() throws Exception {

        Course course = new Course();
        course.setName("course");
        course.setDescription("descr");
        course.setCategory("category");
        course.setLessons(new ArrayList<>());
        course.setSupervisorUsername("petr");
        course.setStudentsCount(20);
        course.setType("offline");

        String expectedContent = objectMapper.writeValueAsString(course);

        this.mockMvc.perform(get("/api/courses/course")
                        .with(user("user").password("user").roles("TEACHER")))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('petr', 'Петров Петр Петрович', 30)")
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('category', null)")
    @Sql(statements = "insert into courses (name, description, category, supervisorUsername, studentsCount, type)" +
            " values ('course', 'descr', 'category', 'petr', 20, 'offline' )")
    void getCourseByNameSuccessForAdmin() throws Exception {

        Course course = new Course();
        course.setName("course");
        course.setDescription("descr");
        course.setCategory("category");
        course.setLessons(new ArrayList<>());
        course.setSupervisorUsername("petr");
        course.setStudentsCount(20);
        course.setType("offline");

        String expectedContent = objectMapper.writeValueAsString(course);

        this.mockMvc.perform(get("/api/courses/course")
                        .with(user("admin").password("admin").roles("ADMIN")))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }


    @Test
    @Sql(statements = "insert into users (username, password) values ('admin', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('petr', 'Петров Петр Петрович', 30)")
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('category', null)")
    void createCourseSuccessForAdmin() throws Exception {

        Course course = new Course();
        course.setName("course");
        course.setDescription("descr");
        course.setCategory("category");
        course.setLessons(new ArrayList<>());
        course.setSupervisorUsername("petr");
        course.setStudentsCount(20);
        course.setType("offline");

        String requestContent = objectMapper.writeValueAsString(course);

        this.mockMvc.perform(post("/api/courses/createCourse")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestContent)
                .with(user("admin").password("admin").roles("ADMIN"))
        ).andExpect(status().isCreated());
    }

    @Test
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('petr', 'Петров Петр Петрович', 30)")
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('category', null)")
    void createCourseSuccessForUser() throws Exception {

        Course course = new Course();
        course.setName("course");
        course.setDescription("descr");
        course.setCategory("category");
        course.setLessons(new ArrayList<>());
        course.setSupervisorUsername("petr");
        course.setStudentsCount(20);
        course.setType("offline");

        String requestContent = objectMapper.writeValueAsString(course);

        this.mockMvc.perform(post("/api/courses/createCourse")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestContent)
                .with(user("petr").password("user").roles("TEACHER"))
        ).andExpect(status().isCreated());
    }

    @Test
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('petr', 'Петров Петр Петрович', 30)")
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('category', null)")
    void createCourseFailedForUser() throws Exception {

        Course course = new Course();
        course.setName("course");
        course.setDescription("descr");
        course.setCategory("category");
        course.setLessons(new ArrayList<>());
        course.setSupervisorUsername("admin");
        course.setStudentsCount(20);
        course.setType("offline");

        String requestContent = objectMapper.writeValueAsString(course);

        this.mockMvc.perform(post("/api/courses/createCourse")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestContent)
                .with(user("petr").password("user").roles("TEACHER"))
        ).andExpect(status().isForbidden());
    }

    @Test@Sql(statements = "insert into users (username, password) values ('admin', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('petr', 'Петров Петр Петрович', 30)")
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('category', null)")
    @Sql(statements = "insert into courses (name, description, category, supervisorUsername, studentsCount, type)" +
            " values ('course', 'descr', 'category', 'petr', 20, 'offline' )")
    void createByCopyingSuccessForUser() throws Exception {
        this.mockMvc.perform(post("/api/courses/createCourseByCopying/course/copiedCourse")
                .with(user("petr").password("user").roles("TEACHER"))
        ).andExpect(status().isCreated());
    }

    @Test
    @Sql(statements = "insert into users (username, password) values ('admin', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('petr', 'Петров Петр Петрович', 30)")
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('category', null)")
    @Sql(statements = "insert into courses (name, description, category, supervisorUsername, studentsCount, type)" +
            " values ('course', 'descr', 'category', 'petr', 20, 'offline' )")
    void updateSuccessForAdmin() throws Exception {
        Course course = new Course();
        course.setName("course");
        course.setDescription("descr");
        course.setLessons(new ArrayList<>());
        course.setCategory("category");
        course.setStudentsCount(30);
        course.setSupervisorUsername("petr");
        course.setType("offline");

        String requestContent = objectMapper.writeValueAsString(course);

        this.mockMvc.perform(put("/api/courses/update")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestContent)
                .with(user("admin").password("admin").roles("ADMIN"))
        ).andExpect(status().isOk());
    }

    @Test
    @Sql(statements = "insert into users (username, password) values ('admin', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into users (username, password) values ('igor', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('petr', 'Петров Петр Петрович', 30)")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('igor', 'Петров Игорь Петрович', 30)")
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('category', null)")
    @Sql(statements = "insert into courses (name, description, category, supervisorUsername, studentsCount, type)" +
            " values ('course', 'descr', 'category', 'igor', 20, 'offline' )")
    void updateForbiddenForUser() throws Exception {
        Course course = new Course();
        course.setName("course");
        course.setDescription("descr");
        course.setLessons(new ArrayList<>());
        course.setCategory("category");
        course.setStudentsCount(30);
        course.setSupervisorUsername("petr");
        course.setType("offline");

        String requestContent = objectMapper.writeValueAsString(course);

        this.mockMvc.perform(put("/api/courses/update")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestContent)
                .with(user("petr").password("user").roles("TEACHER"))
        ).andExpect(status().isForbidden());
    }


    @Test
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('petr', 'Петров Петр Петрович', 30)")
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('category', null)")
    @Sql(statements = "insert into courses (name, description, category, supervisorUsername, studentsCount, type)" +
            " values ('course', 'descr', 'category', 'petr', 20, 'offline' )")
    void deleteForbiddenForUser() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.delete("/api/courses/deleteAll")
                        .with(user("petr").password("user").roles("TEACHER")))
                .andExpect(status().isForbidden());
    }

    @Test
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('petr', 'Петров Петр Петрович', 30)")
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('category', null)")
    @Sql(statements = "insert into courses (name, description, category, supervisorUsername, studentsCount, type)" +
            " values ('course', 'descr', 'category', 'petr', 20, 'offline' )")
    void deleteSuccessForAdmin() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.delete("/api/courses/deleteAll")
                        .with(user("admin").password("admin").roles("ADMIN")))
                .andExpect(status().isOk());
    }

    @Test
    @Sql(statements = "insert into users (username, password) values ('admin', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('petr', 'Петров Петр Петрович', 30)")
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('category', null)")
    @Sql(statements = "insert into courses (name, description, category, supervisorUsername, studentsCount, type)" +
            " values ('course', 'descr', 'category', 'petr', 20, 'offline' )")
    void deleteByNameSuccessForAdmin() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.delete("/api/courses/deleteByName/course")
                        .with(user("admin").password("admin").roles("ADMIN")))
                .andExpect(status().isOk());
    }

    @Test
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('petr', 'Петров Петр Петрович', 30)")
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('category', null)")
    @Sql(statements = "insert into courses (name, description, category, supervisorUsername, studentsCount, type)" +
            " values ('course', 'descr', 'category', 'petr', 20, 'offline' )")
    void deleteByNameSuccessForUser() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.delete("/api/courses/deleteByName/course")
                        .with(user("petr").password("user").roles("ADMIN")))
                .andExpect(status().isOk());
    }

    @Test
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into users (username, password) values ('igor', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('petr', 'Петров Петр Петрович', 30)")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('igor', 'Петров Игорь Петрович', 30)")
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('category', null)")
    @Sql(statements = "insert into courses (name, description, category, supervisorUsername, studentsCount, type)" +
            " values ('course', 'descr', 'category', 'petr', 20, 'offline' )")
    void deleteByNameForbiddenForUser() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.delete("/api/courses/deleteByName/course")
                        .with(user("igor").password("user").roles("ADMIN")))
                .andExpect(status().isForbidden());
    }


    @Test
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('petr', 'Петров Петр Петрович', 30)")
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('category', null)")
    @Sql(statements = "insert into courses (name, description, category, supervisorUsername, studentsCount, type)" +
            " values ('course', 'descr', 'category', 'petr', 20, 'offline' )")
    @Sql(statements = "insert into lessons (id, name, description, teacherUsername, dateTime, courseName) " +
            "values (1, 'lesson', 'descr', 'petr', '', 'course')")
    void addLessonToCourseSuccess()throws Exception {
        this.mockMvc.perform(put("/api/courses/addLessonToCourse/course/1")
                .with(user("petr").password("user").roles("TEACHER"))
        ).andExpect(status().isOk());
    }

    @Test
    @Sql(statements = "insert into users (username, password) values ('igor', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('igor', 'Петров Петр Петрович', 30)")
    @Sql(statements = "insert into users (username, password) values ('petr', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW')")
    @Sql(statements = "insert into teachers (username, fullName, age) values ('petr', 'Петров Петр Петрович', 30)")
    @Sql(statements = "insert into categories (categoryName, parentCategory) values ('category', null)")
    @Sql(statements = "insert into courses (name, description, category, supervisorUsername, studentsCount, type)" +
            " values ('course', 'descr', 'category', 'igor', 20, 'offline' )")
    @Sql(statements = "insert into lessons (id, name, description, teacherUsername, dateTime, courseName) " +
            "values (1, 'lesson', 'descr', 'petr', '', 'course')")
    void addLessonToCourseForbidden()throws Exception {
        this.mockMvc.perform(put("/api/courses/addLessonToCourse/course/1")
                .with(user("petr").password("user").roles("TEACHER"))
        ).andExpect(status().isForbidden());
    }
}