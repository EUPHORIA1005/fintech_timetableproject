package timetableProject.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import timetableProject.model.Course;
import timetableProject.model.Lesson;
import timetableProject.model.Teacher;
import timetableProject.model.User;
import timetableProject.service.LessonService;
import timetableProject.service.TeacherService;
import timetableProject.service.UserService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class LessonController {
    private final LessonService lessonService;
    private final UserService userService;
    private final TeacherService teacherService;

    @GetMapping("/lessons")
    public ResponseEntity<List<Lesson>> getAllLessons() {
        try {
            List<Lesson> lessons = lessonService.findAll();
            if (lessons.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(lessons, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/lessons/{id}")
    public ResponseEntity<Lesson> getLessonById(@PathVariable("id") long id) {
        Lesson lesson = lessonService.findById(id);
        if (lesson != null) {
            return new ResponseEntity<>(lesson, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/lessons/createLesson")
    public ResponseEntity<String> createLesson(@RequestBody @Valid Lesson lesson) {
        try {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            UserDetails userDetails = (UserDetails) auth.getPrincipal();
            User user = userService.findByUsername(userDetails.getUsername());
            if (user.getUsername().equals("admin")) {
                lessonService.save(lesson);
                return new ResponseEntity<>("Занятие было успешно создано", HttpStatus.CREATED);
            }
            Teacher teacher = teacherService.findByUsername(user.getUsername());
            Optional<Course> lessonCourse = teacher.getCourses().stream()
                    .filter(c -> c.getName().equals(lesson.getCourseName())).findAny();
            if (lessonCourse.isPresent()) {
                lessonService.save(lesson);
                return new ResponseEntity<>("Занятие было успешно создано", HttpStatus.CREATED);
            } else {
                return new ResponseEntity<>("Создаваемое вами занятие должно прикрепляться к одному из ваших курсов", HttpStatus.FORBIDDEN);
            }
        } catch (Exception e) {
            return new ResponseEntity<>("Не удается создать занятие", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/lessons/editLesson")
    public ResponseEntity<String> updateLesson(@RequestBody @Valid Lesson lesson) {
        try {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            UserDetails userDetails = (UserDetails) auth.getPrincipal();
            User user = userService.findByUsername(userDetails.getUsername());
            if (user.getUsername().equals("admin")) {
                lessonService.update(lesson);
                return new ResponseEntity<>("Занятие успешно обновлено", HttpStatus.OK);
            }
            Teacher teacher = teacherService.findByUsername(user.getUsername());
            Optional<Course> newLessonCourse = teacher.getCourses().stream()
                    .filter(c -> c.getName().equals(lesson.getCourseName())).findAny();
            Lesson previousVersionOfLesson = lessonService.findById(lesson.getId());
            Optional<Course> previousLessonCourse = teacher.getCourses().stream()
                    .filter(c -> c.getName().equals(previousVersionOfLesson.getCourseName())).findAny();
            if (previousLessonCourse.isPresent() && newLessonCourse.isPresent()) {
                lessonService.update(lesson);
                return new ResponseEntity<>("Занятие успешно обновлено", HttpStatus.OK);
            } else {
                return new ResponseEntity<>("Вы можете редактировать занятия только в рамках своих курсов", HttpStatus.FORBIDDEN);
            }
        } catch (Exception e) {
            return new ResponseEntity<>("Не удается обновить занятие", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @DeleteMapping("/lessons/deleteById/{id}")
    public ResponseEntity<String> deleteLesson(@PathVariable("id") long id) {
        try {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            UserDetails userDetails = (UserDetails) auth.getPrincipal();
            User user = userService.findByUsername(userDetails.getUsername());
            if (user.getUsername().equals("admin")) {
                lessonService.deleteById(id);
                return new ResponseEntity<>("Занятие успешно удалено", HttpStatus.OK);
            }
            Teacher teacher = teacherService.findByUsername(user.getUsername());
            Lesson lesson = lessonService.findById(id);
            Optional<Course> lessonCourse = teacher.getCourses().stream()
                    .filter(c -> c.getName().equals(lesson.getCourseName())).findAny();
            if (lessonCourse.isPresent()) {
                lessonService.deleteById(id);
                return new ResponseEntity<>("Занятие успешно удалено", HttpStatus.OK);
            }
            return new ResponseEntity<>("Вы не можете удалить это занятие", HttpStatus.FORBIDDEN);
        } catch (Exception e) {
            return new ResponseEntity<>("Не удется удалить занятие", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/lessons/deleteAll")
    public ResponseEntity<String> deleteAllLessons() {
        try {
            lessonService.deleteAll();
            return new ResponseEntity<>("Все занятия успешно удалены", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Не удаетя удалить занятия", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
