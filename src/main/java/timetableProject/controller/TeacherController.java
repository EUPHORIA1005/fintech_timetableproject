package timetableProject.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import timetableProject.model.Lesson;
import timetableProject.model.Teacher;
import timetableProject.service.LessonService;
import timetableProject.service.TeacherService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class TeacherController {
    private final TeacherService teacherService;
    private final LessonService lessonService;

    @GetMapping("/teachers")
    public ResponseEntity<List<Teacher>> getAllTeachers(@RequestParam(required = false) Integer age) {
        try {
            List<Teacher> teachers = teacherService.findAll();
            if (teachers.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(teachers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @GetMapping("/teachers/{username}")
    public ResponseEntity<Teacher> getTeacherByUsername(@PathVariable("username") String username) {
        Teacher teacher = teacherService.findByUsername(username);
        if (teacher != null) {
            return new ResponseEntity<>(teacher, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/teachers/timetable/{username}")
    public ResponseEntity<List<Lesson>> getTeacherTimetable(@PathVariable("username") String username){
        try {
            List<Lesson> timetable = lessonService.findByTeacherUsername(username);
            if (!timetable.isEmpty()) {
                return new ResponseEntity<>(timetable, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping("/teachers/createTeacher")
    public ResponseEntity<String> createTeacher(@RequestBody @Valid Teacher teacher) {
        try {
            teacherService.save(teacher);
            return new ResponseEntity<>("Преподаватель успешно создан.", HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>("Не удается создать преподавателя.", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/teachers/updateTeacher")
    public ResponseEntity<String> updateTeacher(@RequestBody @Valid Teacher teacher) {
        try {
            teacherService.update(teacher);
            return new ResponseEntity<>("Преподаватель успешно обновлен.", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Не обновить удалить преподавателя.", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/teachers/{username}")
    public ResponseEntity<String> deleteTeacher(@PathVariable("username") String username) {
        try {
            teacherService.deleteByUsername(username);
            return new ResponseEntity<>("Преподаватель успешно удален.", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Не удается удалить преподавателя.", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/teachers")
    public ResponseEntity<String> deleteAllTeachers() {
        try {
            teacherService.deleteAll();
            return new ResponseEntity<>("Преподаватели успешно удалены.", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Не удается удалить преподавателей.", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
