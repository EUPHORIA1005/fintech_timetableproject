package timetableProject.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import timetableProject.model.RegistrationData;
import timetableProject.model.Teacher;
import timetableProject.model.User;
import timetableProject.service.TeacherService;
import timetableProject.service.UserService;

import javax.validation.Valid;


@RestController
@RequestMapping("/registration")
@RequiredArgsConstructor
public class RegistrationController {

    private final UserService userService;
    private final TeacherService teacherService;

    @PostMapping
    public ResponseEntity<String> register(@RequestBody @Valid RegistrationData data){
        try {
            userService.loadUserByUsername(data.getUser().getUsername());
        }
        catch (UsernameNotFoundException ex){
            User user = data.getUser();
            Teacher teacher = data.getTeacher();
            teacher.setUsername(user.getUsername());
            userService.save(new User(user.getUsername(), new BCryptPasswordEncoder().encode(user.getPassword())));
            teacherService.save(teacher);
            return new ResponseEntity<>("Регистрация прошла успешно", HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
