package timetableProject.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import timetableProject.model.Category;
import timetableProject.service.CategoryService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class CategoryController {
    private final CategoryService categoryService;

    @GetMapping("/categories")
    public ResponseEntity<List<Category>> getCategories() {
        try {
            List<Category> categories = categoryService.findAll();
            if (!categories.isEmpty()) {
                return new ResponseEntity<>(categories, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/categories")
    public ResponseEntity<String> createCategory(@RequestBody @Valid Category category) {
        try {
            categoryService.save(category);
            return new ResponseEntity<>("Категория создана", HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>("Не удалось создать категорию", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/categories")
    public ResponseEntity<String> updateCategory(@RequestBody @Valid Category category) {
        try {
            categoryService.update(category);
            return new ResponseEntity<>("Категория обновлена", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Не удалось обновить категорию", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/categories/{name}")
    public ResponseEntity<String> deleteCategory(@PathVariable("name") String name) {
        try {
            categoryService.deleteCategory(name);
            return new ResponseEntity<>("Категория удалена", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Не удается удалить категорию", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
