package timetableProject.repository;

import org.apache.ibatis.annotations.Mapper;
import timetableProject.model.Course;

import java.util.List;

@Mapper
public interface CourseRepository {
    void save(Course course);
    void update(Course course);
    Course findByName(String name);
    void deleteByName(String name);
    List<Course> findAll();
    void deleteAll();
}
