package timetableProject.repository;

import org.apache.ibatis.annotations.Mapper;
import timetableProject.model.Teacher;
import java.util.List;


@Mapper
public interface TeacherRepository {
    void save(Teacher teacher);
    void update(Teacher teacher);
    Teacher findByUsername(String username);
    void deleteByUsername(String username);
    List<Teacher> findAll();
    void deleteAll();
}
