package timetableProject.repository;

import org.apache.ibatis.annotations.Mapper;
import timetableProject.model.User;

@Mapper
public interface UserRepository {
    User findByUsername(String username);
    void save(User user);
    void deleteAll();
}
