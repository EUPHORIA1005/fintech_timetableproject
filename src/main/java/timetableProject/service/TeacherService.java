package timetableProject.service;

import org.springframework.stereotype.Service;
import timetableProject.model.Teacher;
import timetableProject.repository.TeacherRepository;

import java.util.List;

@Service
public class TeacherService {
    private final TeacherRepository teacherRepository;

    public TeacherService (TeacherRepository repository){
        this.teacherRepository = repository;
    }

    public void save(Teacher teacher){
        teacherRepository.save(teacher);
    }

    public void update(Teacher teacher){
        teacherRepository.update(teacher);
    }

    public Teacher findByUsername(String username){
        return teacherRepository.findByUsername(username);
    }

    public void deleteByUsername(String username){
        teacherRepository.deleteByUsername(username);
    }

    public List<Teacher> findAll(){
        return teacherRepository.findAll();
    }

    public void deleteAll(){
        teacherRepository.deleteAll();
    }
}
