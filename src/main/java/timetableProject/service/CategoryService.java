package timetableProject.service;

import org.springframework.stereotype.Service;
import timetableProject.model.Category;
import timetableProject.repository.CategoryRepository;

import java.util.List;

@Service
public class CategoryService {
    private final CategoryRepository repository;

    public CategoryService(CategoryRepository repository){
        this.repository = repository;
    }

    public void save(Category category){
        repository.save(category);
    }

    public void update(Category category){
        repository.update(category);
    }

    public List<Category> findAll(){
        return repository.findAll();
    }

    public void deleteCategory(String name){
        repository.deleteCategory(name);
    }

    public void deleteAll(){
        repository.deleteAll();
    }
}
