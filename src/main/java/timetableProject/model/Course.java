package timetableProject.model;

import lombok.*;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Модель курса
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Course {
    /**
     * название
     */
    @Getter
    @Setter
    @NotNull(message="Название курса не может быть пустым")
    private String name;
    /**
     * описание
     */
    @Setter
    @Getter
    @NotNull(message="Описание курса не может быть пустым")
    private String description;
    /**
     * категория
     */
    @Setter
    @Getter
    @NotNull(message="Категория курса не может быть пустой")
    private String category;
    /**
     * id куратора
     */
    @Setter
    @Getter
    @NotNull(message="У курса должен быть куратор")
    private String supervisorUsername;
    /**
     * количество студентов
     */
    @Setter
    @Getter
    @NotNull(message="Количество студентов не может быть пустым")
    private Integer studentsCount;
    /**
     * список занятий
     */
    @Getter
    @Setter
    private List<Lesson> lessons;

    /**
     * тип курса(online/offline)
     */
    @Setter
    @Getter
    @NotNull(message="Тип курса не может быть пустым")
    private String type;
}
