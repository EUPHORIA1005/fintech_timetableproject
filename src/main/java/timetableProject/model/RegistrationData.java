package timetableProject.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

public class RegistrationData{
    @Getter
    @Setter
    @NotNull
    private User user;

    @Getter
    @Setter
    @NotNull
    private Teacher teacher;
}
