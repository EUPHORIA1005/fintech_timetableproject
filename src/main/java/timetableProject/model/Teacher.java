package timetableProject.model;

import lombok.*;

import javax.validation.constraints.NotNull;
import java.util.List;


/**
 * Модель преподавателя
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Teacher {

    /**
     * username преподавателя
     */
    @Setter
    @Getter
    @NotNull(message = "username не может быть пустым")
    private String username;

    /**
     * фио
     */
    @Getter
    @Setter
    @NotNull(message = "фио преподавателя не может быть пустым")
    private String fullName;

    /**
     * возраст
     */
    @Getter
    @Setter
    @NotNull(message = "Возраст преподавтеля не может быть пустым")
    private Integer age;

    /**
     * список курсов
     */
    @Getter
    @Setter
    private List<Course> courses;

}
