package timetableProject.model;

import lombok.*;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    @NotNull(message = "Имя пользователя не может быть пустым")
    private String username;
    @NotNull(message = "Пароль не может быть пустым")
    private String password;
}
