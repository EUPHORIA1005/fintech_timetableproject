CREATE TABLE IF NOT EXISTS users
(
    username TEXT PRIMARY KEY,
    password TEXT
);

CREATE TABLE IF NOT EXISTS teachers
(
    username TEXT PRIMARY KEY,
    fullName TEXT,
    age      INT,
    CONSTRAINT FK_teacher_user FOREIGN KEY (username) REFERENCES users (username)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS categories
(
    categoryName   TEXT PRIMARY KEY,
    parentCategory TEXT,
    CONSTRAINT FK_parent_child FOREIGN KEY (parentCategory) REFERENCES categories (categoryName)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS courses
(
    name               TEXT PRIMARY KEY,
    description        TEXT,
    category           TEXT,
    supervisorUsername TEXT,
    studentsCount      INT,
    type               TEXT,
    CONSTRAINT FK_course_supervisor FOREIGN KEY (supervisorUsername) REFERENCES teachers (username)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    CONSTRAINT FK_course_category FOREIGN KEY (category) REFERENCES categories (categoryName)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS lessons
(
    id              BIGSERIAL,
    name            TEXT,
    description     TEXT,
    teacherUsername TEXT,
    dateTime        TEXT,
    courseName      TEXT,
    CONSTRAINT FK_lessons_teachers FOREIGN KEY (teacherUsername) REFERENCES teachers (username)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    CONSTRAINT FK_lessons_courses FOREIGN KEY (courseName) REFERENCES courses (name)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

INSERT INTO users (username, password)
VALUES ('admin', '$2a$10$gnpqf51/1FymMUtpi02ZTOoIhbanxrDkNtRCziL3rdJSj2p2aiufW');

INSERT INTO categories (categoryName, parentCategory)
VALUES ('технические', null);

INSERT INTO categories (categoryName, parentCategory)
VALUES ('математика', 'технические');

INSERT INTO categories (categoryName, parentCategory)
VALUES ('языки программирования', 'технические');

INSERT INTO categories (categoryName, parentCategory)
VALUES ('инструменты и технологии', 'технические');

INSERT INTO categories (categoryName, parentCategory)
VALUES ('soft skills', null);

INSERT INTO categories (categoryName, parentCategory)
VALUES ('иностранные языки', 'soft skills');

INSERT INTO categories (categoryName, parentCategory)
VALUES ('деловое общение', 'soft skills');
